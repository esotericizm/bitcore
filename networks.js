var Put = require('bufferput');
var buffertools = require('buffertools');
var hex = function(hex) {return new Buffer(hex, 'hex');};

exports.livenet = {
  name: 'livenet',
  magic: hex('fec3b9de'),
  addressVersion: 28,
  privKeyVersion: 156
  P2SHVersion: 5,
  hkeyPublicVersion: 0x0488b21e,
  hkeyPrivateVersion: 0x0488ade4,
  genesisBlock: {
    hash: hex('8d18f93b1eb159f6bb3e5505bc3fb9c0a692e86c373033771e67cef7100a0000'),
    merkle_root: hex('5845365c3dc08f08b3c5c50f4bbccb8c556dfa65cfbcd629df6af944a40d4b2a'),
    height: 0,
    nonce: 325433,
    version: 1,
    prev_hash: buffertools.fill(new Buffer(32), 0),
    timestamp: 1400408750,
    bits: 0x1e0ffff0,
  },
  dnsSeeds: [
    'seed.cannabiscoin.net',
			'69.76.9.161',
			'71.209.219.176',
			'78.55.121.246',
			'71.252.144.102',
  ],
  defaultClientPort: 39348
};

exports.testnet = {
  name: 'testnet',
  magic: hex('0711050b'),
  addressVersion: 0x7f,
  privKeyVersion: 255,
  P2SHVersion: 196,
  hkeyPublicVersion: 0x043587cf,
  hkeyPrivateVersion: 0x04358394,
  genesisBlock: {
    hash: hex('726fad846b2d5101883f0d5fea8f732d8a49ec0828c8ed475238c1870a910000'),
    merkle_root: hex('26a3ff5d3dc46b091e7b58b6022982e6d27dff1bab3bd1da6beb4790983c87c4'),
    height: 0,
    nonce: 55887,
    version: 1,
    prev_hash: buffertools.fill(new Buffer(32), 0),
    timestamp: 1405769613,
    bits: 0x1f00ffff,
  },
  dnsSeeds: [
    'testnet-seed.sdcoin.co'
  ],
  defaultClientPort: 51997
};
